import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { AttachedImagesListComponent } from './attached-images/attached-images-list.component';
import { AttachedImageComponent } from './attached-images/attached-image.component';


@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    AttachedImagesListComponent,
    AttachedImageComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
