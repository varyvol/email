import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { EmailValidators } from './validators/emails-validators';
import { AttachedImagesService } from '../shared/attached-images.service';

declare var $: any;

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css'],
  providers: [AttachedImagesService]
})
export class FormComponent implements OnInit {

  emailForm: FormGroup;

  constructor(private imagesService: AttachedImagesService) { }

  ngOnInit() {
    this.displayModal();
    this.configureForm();
  }

  private displayModal(): void {
    $('#emailModal').modal({
      'backdrop': false,
      'keyboard': false,
      'show': true
    });
  }

  private configureForm(): void {
    /* Each control has its own validator. The
    the group for all the email controls has an extra one */
    this.emailForm = new FormGroup({
      'emailAddresses': new FormGroup({
        'to': new FormControl(null, [EmailValidators.emailsList]),
        'cc': new FormControl(null, [EmailValidators.emailsList]),
        'bcc': new FormControl(null, [EmailValidators.emailsList])
      }, EmailValidators.emailReceivers),
      'subject': new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      'message': new FormControl(null, [Validators.required])
    });
  }

  sendEmail() {
    this.emailForm.reset();
    this.imagesService.removeAll();
  }

  sendButtonClass() {
    return {
      'gray': !this.emailForm.valid,
      'btn-info': this.emailForm.valid
    };
  }

}
