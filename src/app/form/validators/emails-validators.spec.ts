import { FormControl, ValidationErrors } from '@angular/forms';
import { EmailValidators } from './emails-validators';

const getValidation = function(content: string): ValidationErrors {
    const c: FormControl = new FormControl(content);
    return EmailValidators.emailsList(c);
};

const valid = function(content: string): void {
    expect(getValidation(content)).toBeNull();
};

const invalid = function(content: string): void {
    const res: ValidationErrors = getValidation(content);
    expect(res).not.toBeNull();
    expect(res['listOfEmails']).toBeTruthy();
};

describe('EmailValidators', () => {

    it('approves empty email lists', () => {
        valid(null);
        valid('');
    });

    it('detects a wrong email list', () => {
        invalid('a');
        invalid('a@');
        invalid('a@gmail.com,');
    });

    it('marks one email as valid', () => {
        valid('one@gmail.com');
    });

    it('marks two emails list as valid', () => {
        valid('one@gmail.com, two@gmail.com');
        valid(' one@gmail.com,two@gmail.com');
        valid(' one@gmail.com ,two@gmail.com ');
    });
});
