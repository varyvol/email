import { FormControl, Validators, FormGroup, ValidationErrors } from '@angular/forms';
import { AbstractControl } from '@angular/forms/src/model';

export class EmailValidators {

    public static emailsList(control: FormControl): ValidationErrors {
        // Check for content, since we don't want to force the email to be required
        if (EmailValidators.controlHasContent(control)) {
            const emails: string[] = control.value.split(',');

            // Find any invalid email
            const invalidEmails: FormControl[] = emails
                .map(email => new FormControl(email.trim()))
                .filter(emailControl => EmailValidators.isEmailInvalid(emailControl));

            if (invalidEmails.length !== 0) {
                return {'listOfEmails': true};
            }
        }

        return null;
    }

    private static controlHasContent(control: FormControl) {
        const controlValue: string = control.value;
        return controlValue != null && controlValue.length !== 0;
    }

    private static isEmailInvalid(emailControl: FormControl): boolean {
        return Validators.email(emailControl) != null;
    }

    public static emailReceivers(group: FormGroup): ValidationErrors {
        // Get all email-related controls in the group
        const controls: AbstractControl[] = Object.values(group.controls);

        if (!EmailValidators.allValid(controls) || !EmailValidators.atLeastOneWithContent(controls)) {
            return {'emailReceivers': true};
        }

        return null;
    }

    private static atLeastOneWithContent(controls: AbstractControl[]): boolean {
        return controls
            .some(c => Validators.required(c) == null);
    }

    private static allValid(controls: AbstractControl[]): boolean {
        return controls
            .every(c => c.valid);
    }

}
