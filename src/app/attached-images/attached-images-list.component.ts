import { Component, OnInit } from '@angular/core';

import { AttachedImagesService } from '../shared/attached-images.service';

@Component({
  selector: 'app-attached-images',
  templateUrl: './attached-images-list.component.html',
  styleUrls: ['./attached-images-list.component.css']
})
export class AttachedImagesListComponent implements OnInit {

  private attachedImages: string[];

  constructor(private imagesService: AttachedImagesService) { }

  ngOnInit() {
    // Gather the initial images list and subscribe to changes
    this.storeAttachedImages(this.imagesService.getAttachedImages());
    this.imagesService.attachedImagesChanged.subscribe(
      (images: string[]) => this.storeAttachedImages(images)
    );
  }

  private storeAttachedImages(images: string[]) {
    this.attachedImages = images;
  }

  imagesListClass() {
    return {
      'with-content': this.attachedImages.length > 0
    };
  }

  changeFile($event: any): void {
    const files: File[] = $event.target.files;
    const file: File = files[0];
    this.loadFileData(file);

    // Reset the input field value
    $event.target.value = null;
  }

  private loadFileData(file: File): void {
    const fileReader: FileReader = new FileReader();

    fileReader.onloadend = (loadEndEvent) => {
      this.imagesService.addImage(fileReader.result);
    };

    fileReader.readAsDataURL(file);
  }

}
