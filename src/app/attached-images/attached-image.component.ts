import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

import { AttachedImagesService } from '../shared/attached-images.service';

@Component({
  selector: 'app-attached-image',
  templateUrl: './attached-image.component.html',
  styleUrls: ['./attached-image.component.css']
})
export class AttachedImageComponent implements OnInit {

  @Input('image') image;
  @Input('index') index;

  constructor(private domSanitizer: DomSanitizer, private imagesService: AttachedImagesService) { }

  ngOnInit() {
  }

  removeImage() {
    this.imagesService.removeImage(this.index);
  }

}
