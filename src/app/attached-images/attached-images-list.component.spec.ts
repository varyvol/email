import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { AttachedImagesListComponent } from './attached-images-list.component';
import { AttachedImageComponent } from './attached-image.component';
import { AttachedImagesService } from '../shared/attached-images.service';
import { By } from '@angular/platform-browser';

const getNumberOfImages = function(fixture: ComponentFixture<AttachedImagesListComponent>): number {
  return fixture.debugElement.queryAllNodes(By.css('img')).length;
};

describe('AttachedImagesComponent', () => {
  let component: AttachedImagesListComponent;
  let fixture: ComponentFixture<AttachedImagesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachedImagesListComponent, AttachedImageComponent ],
      providers: [AttachedImagesService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachedImagesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render all attached images', inject([AttachedImagesService], (service: AttachedImagesService) => {
    expect(getNumberOfImages(fixture)).toBe(0);

    service.addImage('a');
    fixture.detectChanges();
    expect(getNumberOfImages(fixture)).toBe(1);

    service.addImage('b');
    fixture.detectChanges();
    expect(getNumberOfImages(fixture)).toBe(2);
  }));
});
