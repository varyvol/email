import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AttachedImagesService {

  private attachedImages: string[] = [];
  attachedImagesChanged: Subject<String[]> = new Subject();

  constructor() { }

  getAttachedImages(): string[] {
    return this.unmodifiableAttachedImages();
  }

  addImage(image: string): void {
    this.attachedImages.push(image);
    this.notifyAttachedImagesChanges();
  }

  removeImage(index: number): void {
    this.attachedImages.splice(index, 1);
    this.notifyAttachedImagesChanges();
  }

  removeAll(): void {
    this.attachedImages = [];
    this.notifyAttachedImagesChanges();
  }

  private notifyAttachedImagesChanges(): void {
    this.attachedImagesChanged.next(this.unmodifiableAttachedImages());
  }

  private unmodifiableAttachedImages(): string[] {
    // Return a copy of the array so that the original one cannot be modified
    return this.attachedImages.slice();
  }

}
