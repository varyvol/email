import { TestBed, inject } from '@angular/core/testing';

import { AttachedImagesService } from './attached-images.service';

// The service does not validate since we expected the entry to be a proper image
const FIRST_IMAGE = 'firstImage';
const SECOND_IMAGE = 'secondImage';

const size = function(service: AttachedImagesService): number {
  return service.getAttachedImages().length;
};

const includeImages = function(service: AttachedImagesService) {
  service.addImage(FIRST_IMAGE);
  service.addImage(SECOND_IMAGE);
  expect(size(service)).toBe(2);
};

describe('AttachedImagesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AttachedImagesService]
    });
  });

  it('should be created', inject([AttachedImagesService], (service: AttachedImagesService) => {
    expect(service).toBeTruthy();
  }));

  it('should return empty initially', inject([AttachedImagesService], (service: AttachedImagesService) => {
    expect(size(service)).toBe(0);
  }));

  it('should add images in the proper order', inject([AttachedImagesService], (service: AttachedImagesService) => {
    includeImages(service);

    const images: string[] = service.getAttachedImages();
    expect(images[0]).toEqual(FIRST_IMAGE);
    expect(images[1]).toEqual(SECOND_IMAGE);
  }));

  it('should remove images according to the index', inject([AttachedImagesService], (service: AttachedImagesService) => {
    includeImages(service);

    service.removeImage(0);
    expect(size(service)).toBe(1);

    const images: string[] = service.getAttachedImages();
    expect(service.getAttachedImages()[0]).toEqual(SECOND_IMAGE);
  }));

  it('should remove all images', inject([AttachedImagesService], (service: AttachedImagesService) => {
    includeImages(service);

    service.removeAll();
    expect(size(service)).toBe(0);
  }));

});
