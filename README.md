# Ebury Front-end task by Evaristo Gutiérrez

## Angular CLI

Project created using Angular-cli to ease the initial setup.

* 'npm install' to install all necessary dependencies. 
* 'ng serve' to start the application.
* 'ng test' to execute the functional tests.
* 'ng e2e' to execute the end to end tests.

## Considerations

* I have styled the applicacion using Bootstrap but I have not paid great attention to the design part. I do not consider myself an expert on that, so I think trying to demonstrate here something I am not is not worth it. The fact of using a modal as well as not implementing the gradient in the background are consequences of this too.

* I have used Angular 2 but I think this is (or should be) about demonstrating how to handle concepts and/or situations, and then the language or framework is just a tool to handle those.

* I have created a utility class, a service to handle information and components in charge of displaying and handling parts of such information. Even though this could have been probably extended, I think the actual state is a good representation of how it should be done: each service/component takes care only of what it really needs to.

* That also eases functional testing as tests for a service/component can isolate from other ones.

* Tests (obviously there should be tests for each model/service/component but I created one for each of the following concepts. These could of course be improved too by adding extra test cases): 

    * Unit tests for a class where Angular is not even involved (EmailValidators tests).
    * Functional tests for a service, demonstrating that the images service is handling the information appropriately.
    * Functional tests for a component, demonstrating that it handles events apropriately and the HTML or the internal state reflect those changes (the attached images are displayed in the form).
    * E2E tests which would simulate a user flow in the complete application. It should not understand of components or services but just check that a user is able to do what is expected to do.
