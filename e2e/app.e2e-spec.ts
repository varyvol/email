import { AppPage } from './app.po';

describe('ebury-app App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should not allow sending until validations are fulfilled', () => {
    page.navigateTo();
    expect(page.isSendButtonEnabled()).toBeFalsy();

    page.typeSubject('any subject');
    page.typeMessage('any message');
    expect(page.isSendButtonEnabled()).toBeFalsy();

    page.typeTo('email@gmail.com');
    expect(page.isSendButtonEnabled()).toBeTruthy();

    page.typeTo('invalidEmail');
    expect(page.isSendButtonEnabled()).toBeFalsy();
  });
});
