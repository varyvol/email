import { browser, by, element, $ } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  isSendButtonEnabled() {
    return $('#sendButton').isEnabled();
  }

  typeMessage(message: string) {
    this.type('#message', message);
  }

  typeSubject(subject: string) {
    this.type('#subject', subject);
  }

  typeTo(address: string) {
    this.type('#to', address);
  }

  private type(elemLocator: string, content: string) {
    const elem = $(elemLocator);
    elem.clear();
    elem.sendKeys(content);
  }
}
